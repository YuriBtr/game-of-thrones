package ru.skillbranch.yuribtr.ui.activities;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.redmadrobot.chronos.ChronosConnector;
import java.util.List;
import ru.skillbranch.yuribtr.R;
import ru.skillbranch.yuribtr.data.database.LoadHousesFromDb;
import ru.skillbranch.yuribtr.data.storage.models.CharacterDTO;
import ru.skillbranch.yuribtr.data.storage.models.House;
import ru.skillbranch.yuribtr.ui.adapters.CharactersAdapter;
import ru.skillbranch.yuribtr.utils.ConstantManager;
import ru.skillbranch.yuribtr.utils.UiHelper;
import ru.skillbranch.yuribtr.data.storage.models.Character;

public class MainActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener{

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ChronosConnector mConnector;
    public List<House> mHouses;
    private CoordinatorLayout mCoordinatorLayout;
    private RecyclerView mRecyclerView;
    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private CharactersAdapter mCharactersAdapter;
    public static String tab1 = "UNDEFINED", tab2 = "UNDEFINED", tab3 = "UNDEFINED";
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        UiHelper.writeLog("onCreate: MainActivity");
        mConnector = new ChronosConnector();
        mConnector.onCreate(this, savedInstanceState);
        setContentView(R.layout.activity_main);

//        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
//        mSwipeRefreshLayout.setOnRefreshListener(this);

        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.main_content);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.navigation_drawer);
        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
//                UiHelper.writeLog("onPageSelected: curTab " + position);
                showHouses(position);
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                switch (tab.getPosition()){
                    case 0:  mNavigationView.getMenu().findItem(R.id.first_tab_menu).setChecked(true);
                        break;
                    case 1:  mNavigationView.getMenu().findItem(R.id.second_tab_menu).setChecked(true);
                        break;
                    case 2:  mNavigationView.getMenu().findItem(R.id.third_tab_menu).setChecked(true);
                        break;
                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        setupToolbar();
        setupDrawer();
        showProgress();
        loadHousesFromDb();
        hideProgress();
    }


    public void loadHousesFromDb() {
        LoadHousesFromDb lb = new LoadHousesFromDb();
        mHouses = lb.getHouses();
        if (mHouses != null && mHouses.size()==3) {
//            UiHelper.writeLog("loadHousesFromDb: mHouses not empty");
            tab1 = mHouses.get(0).getHouseShortName()+"s";
            tab2 = mHouses.get(1).getHouseShortName()+"s";
            tab3 = mHouses.get(2).getHouseShortName()+"s";
            mSectionsPagerAdapter.notifyDataSetChanged();

            MenuItem menuItem1 = mNavigationView.getMenu().findItem(R.id.first_tab_menu);
            MenuItem menuItem2 = mNavigationView.getMenu().findItem(R.id.second_tab_menu);
            MenuItem menuItem3 = mNavigationView.getMenu().findItem(R.id.third_tab_menu);
            menuItem1.setTitle(tab1);
            menuItem2.setTitle(tab2);
            menuItem3.setTitle(tab3);

            if (menuItem1.getTitle().equals("Starks")) menuItem1.setIcon(getResources().getDrawable(R.drawable.stark_icon_xxxsmall));
            else if (menuItem1.getTitle().equals("Lannisters")) menuItem1.setIcon(getResources().getDrawable(R.drawable.lanister_icon_xxxsmall));
            else if (menuItem1.getTitle().equals("Targaryens")) menuItem1.setIcon(getResources().getDrawable(R.drawable.targariyen_icon_xxxsmall));

            if (menuItem2.getTitle().equals("Starks")) menuItem2.setIcon(getResources().getDrawable(R.drawable.stark_icon_xxxsmall));
            else if (menuItem2.getTitle().equals("lannisters")) menuItem2.setIcon(getResources().getDrawable(R.drawable.lanister_icon_xxxsmall));
            else if (menuItem2.getTitle().equals("Targaryens")) menuItem2.setIcon(getResources().getDrawable(R.drawable.targariyen_icon_xxxsmall));

            if (menuItem3.getTitle().equals("Starks")) menuItem3.setIcon(getResources().getDrawable(R.drawable.stark_icon_xxxsmall));
            else if (menuItem3.getTitle().equals("Lannisters")) menuItem3.setIcon(getResources().getDrawable(R.drawable.lanister_icon_xxxsmall));
            else if (menuItem3.getTitle().equals("Targaryens")) menuItem3.setIcon(getResources().getDrawable(R.drawable.targariyen_icon_xxxsmall));


        } else {
//            UiHelper.writeLog("loadHousesFromDb: mHouses empty");
            showSnackbar(getString(R.string.error_internal_message));
        }
    }

    public void showHouses(int position) {
        //UiHelper.writeLog("showHouses: position "+position);
        if (mHouses!=null && position<mHouses.size()) {
            List<Character> tmp = mHouses.get(position).getSwornMembersList();

            mCharactersAdapter = new CharactersAdapter(tmp, new CharactersAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(Character item) {
                    CharacterDTO characterDTO = new CharacterDTO(item);
                    Intent profileIntent = new Intent(MainActivity.this, CharacterActivity.class);
                    profileIntent.putExtra(ConstantManager.PARCELABLE_KEY, characterDTO);
                    startActivity(profileIntent);
                }
            });
            mRecyclerView.swapAdapter(mCharactersAdapter, false);
            setupAdapters();
        } //else UiHelper.writeLog("showHouses: mHouses empty");

    }

    public void setupAdapters () {
//        UiHelper.writeLog("setupAdapters: inside");
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar!=null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }


    private void setupDrawer() {
        mNavigationView.setCheckedItem(R.id.first_tab_menu);

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                                                              @Override
                                                              public boolean onNavigationItemSelected(MenuItem item) {
                                                                  if (item.getItemId()==R.id.first_tab_menu) {
                                                                      mViewPager.setCurrentItem(0);
                                                                  } else
                                                                  if (item.getItemId()==R.id.second_tab_menu) {
                                                                      mViewPager.setCurrentItem(1);
                                                                  } else
                                                                  if (item.getItemId()==R.id.third_tab_menu) {
                                                                      mViewPager.setCurrentItem(2);
                                                                  }
                                                                  item.setChecked(true);
                                                                  mDrawerLayout.closeDrawer(GravityCompat.START);
                                                                  return false;
                                                              }
                                                          }
        );

    }

    @Override
    protected void onResume() {
        super.onResume();
        mConnector.onResume();
    }

    @Override
    protected void onPause() {
        mConnector.onPause();
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mConnector.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        //prevent revert to splash screen
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else moveTaskToBack(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }
        return super.onOptionsItemSelected(item);
    }



    public static class PlaceholderFragment extends Fragment{

        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber-1);
            fragment.setArguments(args);
            return fragment;
        }
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
//            UiHelper.writeLog("onCreateView: inside");
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            MainActivity ma = (MainActivity) getActivity();
            ma.mRecyclerView = (RecyclerView) rootView.findViewById(R.id.character_list);
//            ma.mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.refresh);
//            ma.mSwipeRefreshLayout.setOnRefreshListener(this);
            //ma.mSwipeRefreshLayout.setColorSchemeResources(R.color.blue, R.color.green, R.color.yellow, R.color.red);

            ma.showHouses(getArguments().getInt(ARG_SECTION_NUMBER));
            return rootView;
        }
//
//        @Override
//        public void onRefresh() {
//            MainActivity ma = (MainActivity) getActivity();
//            ma.mSwipeRefreshLayout.setRefreshing(false);
//            ma.mSwipeRefreshLayout.clearAnimation();
//        // говорим о том, что собираемся начать
//        Toast.makeText(ma, R.string.refresh_started, Toast.LENGTH_SHORT).show();
//        // начинаем показывать прогресс
//            ma.showProgress();
//        // ждем 3 секунды и прячем прогресс
//            ma.mSwipeRefreshLayout.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                MainActivity ma = (MainActivity) getActivity();
//                ma.mSwipeRefreshLayout.setRefreshing(false);
//                // говорим о том, что собираемся закончить
//                Toast.makeText(ma, R.string.refresh_finished, Toast.LENGTH_SHORT).show();
//                ma.hideProgress();
//            }
//        }, 3000);
//        }
    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return MainActivity.tab1;
                case 1:
                    return MainActivity.tab2;
                case 2:
                    return MainActivity.tab3;
            }
            return null;
        }
    }

    private void showSnackbar(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(false);
        mSwipeRefreshLayout.clearAnimation();
        // говорим о том, что собираемся начать
        Toast.makeText(this, R.string.refresh_started, Toast.LENGTH_SHORT).show();
        // начинаем показывать прогресс
        showProgress();
        // ждем 3 секунды и прячем прогресс
        mSwipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(false);
                // говорим о том, что собираемся закончить
                Toast.makeText(MainActivity.this, R.string.refresh_finished, Toast.LENGTH_SHORT).show();
                hideProgress();
            }
        }, 3000);
    }
}
