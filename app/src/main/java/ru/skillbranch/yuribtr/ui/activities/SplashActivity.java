package ru.skillbranch.yuribtr.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.redmadrobot.chronos.ChronosConnector;

import ru.skillbranch.yuribtr.R;
import ru.skillbranch.yuribtr.data.database.ChronosSaveHousesToDb;
import ru.skillbranch.yuribtr.data.managers.DataManager;
import ru.skillbranch.yuribtr.utils.SkillBranchApplication;
import ru.skillbranch.yuribtr.utils.UiHelper;

public class SplashActivity extends BaseActivity{
    private ChronosConnector mConnector;
    private DataManager mDataManager;
    private Context mContext;
    private TextView mSplashMessage;
    private Handler handler = new Handler();
    static String message="Приложение запускается. Подождите...";

    public static void UpdateMessage(String newMessage){
        message=newMessage;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        showProgress();
        mContext= SkillBranchApplication.getContext();
        mConnector = new ChronosConnector();
        mConnector.onCreate(this, savedInstanceState);
        mDataManager = DataManager.getInstance();
        mSplashMessage = (TextView) findViewById(R.id.splash_message);
        mSplashMessage.setText(R.string.connecting_to_server_message);
        UiHelper.writeLog(mContext.getString(R.string.connecting_to_server_message));
        //start to load data in separate thread
        mSplashMessage.setText(R.string.houses_list_loading_message);
        mConnector.runOperation(new ChronosSaveHousesToDb(), false);
        handler.postDelayed(runnable, 1000);
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            mSplashMessage.setText(message);
            handler.postDelayed(this, 1000);
        }
    };


    public void onOperationFinished(final ChronosSaveHousesToDb.Result result) {
        hideProgress();
        if (result.isSuccessful()) {
            UiHelper.writeLog(result.getOutput());
            mSplashMessage.setText(result.getOutput());
            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(intent);
        } else {
            //Chronos errors will be here
            UiHelper.writeLog(mContext.getString(R.string.error_internal_message));
            showToast(getString(R.string.error_internal_message)+" at ChronosSaveHousesToDb");
            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onPause() {
        mConnector.onPause();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mConnector.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mConnector.onSaveInstanceState(outState);
    }

}
