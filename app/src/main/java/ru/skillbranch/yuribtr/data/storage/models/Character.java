package ru.skillbranch.yuribtr.data.storage.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.ToOne;
import org.greenrobot.greendao.annotation.Unique;

@Entity(active = true, nameInDb = "CHARACTERS")
public class Character {

    @Id
    private Long id;

    private Long remoteId1;
    
    private Long remoteId2;

    private String houseRemoteId;

    private String houseShortName;

    private String url;

    private String name;

    private String gender;

    private String culture;

    private String born;

    private String died;

    private String titles;

    private String aliases;

    private String father;

    private String mother;

    private long fatherId;

    @ToOne(joinProperty = "remoteId1")
    private Character fatherCharacter;

    private long motherId;

    @ToOne(joinProperty = "remoteId2")
    private Character motherCharacter;

    private String spouse;

    private String allegiances;

    private String books;

    private String povBooks;

    private String tvSeries;

    private String playedBy;

    private String houseWords;


    /** To-one relationship, resolved on first access. */
    @Generated(hash = 984087512)
    public Character getFatherCharacter() {
        Long __key = this.remoteId1;
        if (fatherCharacter__resolvedKey == null || !fatherCharacter__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            CharacterDao targetDao = daoSession.getCharacterDao();
            Character fatherCharacterNew = targetDao.load(__key);
            synchronized (this) {
                fatherCharacter = fatherCharacterNew;
                fatherCharacter__resolvedKey = __key;
            }
        }
        return fatherCharacter;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 600394135)
    public void setFatherCharacter(Character fatherCharacter) {
        synchronized (this) {
            this.fatherCharacter = fatherCharacter;
            remoteId1 = fatherCharacter == null ? null : fatherCharacter.getId();
            fatherCharacter__resolvedKey = remoteId1;
        }
    }

    /** To-one relationship, resolved on first access. */
    @Generated(hash = 940349096)
    public Character getMotherCharacter() {
        Long __key = this.remoteId2;
        if (motherCharacter__resolvedKey == null || !motherCharacter__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            CharacterDao targetDao = daoSession.getCharacterDao();
            Character motherCharacterNew = targetDao.load(__key);
            synchronized (this) {
                motherCharacter = motherCharacterNew;
                motherCharacter__resolvedKey = __key;
            }
        }
        return motherCharacter;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 271788822)
    public void setMotherCharacter(Character motherCharacter) {
        synchronized (this) {
            this.motherCharacter = motherCharacter;
            remoteId2 = motherCharacter == null ? null : motherCharacter.getId();
            motherCharacter__resolvedKey = remoteId2;
        }
    }

    public String getHouseWords() {
        return houseWords;
    }

    public void setHouseWords(String houseWords) {
        this.houseWords = houseWords;
    }

    /** Used for active entity operations. */
    @Generated(hash = 898307126)
    private transient CharacterDao myDao;
    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    @Generated(hash = 1840026637)
    private transient Long motherCharacter__resolvedKey;

    @Generated(hash = 1569398707)
    private transient Long fatherCharacter__resolvedKey;


    public void setHouseShortName(String houseShortName) {
        this.houseShortName = houseShortName;
    }

    public String getHouseShortName() {
        return houseShortName;
    }

    @Generated(hash = 1776527679)
    public Character(Long id, Long remoteId1, Long remoteId2, String houseRemoteId, String houseShortName,
            String url, String name, String gender, String culture, String born, String died, String titles,
            String aliases, String father, String mother, long fatherId, long motherId, String spouse,
            String allegiances, String books, String povBooks, String tvSeries, String playedBy, String houseWords) {
        this.id = id;
        this.remoteId1 = remoteId1;
        this.remoteId2 = remoteId2;
        this.houseRemoteId = houseRemoteId;
        this.houseShortName = houseShortName;
        this.url = url;
        this.name = name;
        this.gender = gender;
        this.culture = culture;
        this.born = born;
        this.died = died;
        this.titles = titles;
        this.aliases = aliases;
        this.father = father;
        this.mother = mother;
        this.fatherId = fatherId;
        this.motherId = motherId;
        this.spouse = spouse;
        this.allegiances = allegiances;
        this.books = books;
        this.povBooks = povBooks;
        this.tvSeries = tvSeries;
        this.playedBy = playedBy;
        this.houseWords = houseWords;
    }

    @Generated(hash = 1853959157)
    public Character() {
    }


    /**
     *
     * @return
     * The url
     */
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     * The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The gender
     */
    public String getGender() {
        return gender;
    }

    /**
     *
     * @param gender
     * The gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     *
     * @return
     * The culture
     */
    public String getCulture() {
        return culture;
    }

    /**
     *
     * @param culture
     * The culture
     */
    public void setCulture(String culture) {
        this.culture = culture;
    }

    /**
     *
     * @return
     * The born
     */
    public String getBorn() {
        return born;
    }

    /**
     *
     * @param born
     * The born
     */
    public void setBorn(String born) {
        this.born = born;
    }

    /**
     *
     * @return
     * The died
     */
    public String getDied() {
        return died;
    }

    /**
     *
     * @param died
     * The died
     */
    public void setDied(String died) {
        this.died = died;
    }

    /**
     *
     * @return
     * The titles
     */
    public String getTitles() {
        return titles;
    }

    /**
     *
     * @param titles
     * The titles
     */
    public void setTitles(String titles) {
        this.titles = titles;
    }

    /**
     *
     * @return
     * The aliases
     */
    public String getAliases() {
        return aliases;
    }

    /**
     *
     * @param aliases
     * The aliases
     */
    public void setAliases(String aliases) {
        this.aliases = aliases;
    }

    /**
     *
     * @return
     * The father
     */
    public String getFather() {
        return father;
    }

    /**
     *
     * @param father
     * The father
     */
    public void setFather(String father) {
        this.father = father;
    }

    /**
     *
     * @return
     * The mother
     */
    public String getMother() {
        return mother;
    }

    /**
     *
     * @param mother
     * The mother
     */
    public void setMother(String mother) {
        this.mother = mother;
    }

    /**
     *
     * @return
     * The spouse
     */
    public String getSpouse() {
        return spouse;
    }

    /**
     *
     * @param spouse
     * The spouse
     */
    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }

    /**
     *
     * @return
     * The allegiances
     */
    public String getAllegiances() {
        return allegiances;
    }

    /**
     *
     * @param allegiances
     * The allegiances
     */
    public void setAllegiances(String allegiances) {
        this.allegiances = allegiances;
    }

    /**
     *
     * @return
     * The books
     */
    public String getBooks() {
        return books;
    }

    /**
     *
     * @param books
     * The books
     */
    public void setBooks(String books) {
        this.books = books;
    }

    /**
     *
     * @return
     * The povBooks
     */
    public String getPovBooks() {
        return povBooks;
    }

    /**
     *
     * @param povBooks
     * The povBooks
     */
    public void setPovBooks(String povBooks) {
        this.povBooks = povBooks;
    }

    /**
     *
     * @return
     * The tvSeries
     */
    public String getTvSeries() {
        return tvSeries;
    }

    /**
     *
     * @param tvSeries
     * The tvSeries
     */
    public void setTvSeries(String tvSeries) {
        this.tvSeries = tvSeries;
    }

    /**
     *
     * @return
     * The playedBy
     */
    public String getPlayedBy() {
        return playedBy;
    }

    /**
     *
     * @param playedBy
     * The playedBy
     */
    public void setPlayedBy(String playedBy) {
        this.playedBy = playedBy;
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 162219484)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getCharacterDao() : null;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHouseRemoteId() {
        return this.houseRemoteId;
    }

    public void setHouseRemoteId(String houseRemoteId) {
        this.houseRemoteId = houseRemoteId;
    }

    public Long getRemoteId1() {
        return this.remoteId1;
    }

    public void setRemoteId1(Long remoteId1) {
        this.remoteId1 = remoteId1;
    }


    public Long getRemoteId2() {
        return this.remoteId2;
    }

    public void setRemoteId2(Long remoteId2) {
        this.remoteId2 = remoteId2;
    }

    public long getMotherId() {
        return this.motherId;
    }


    public long getFatherId() {
        return this.fatherId;
    }


    public void setMotherId(long motherId) {
        this.motherId = motherId;
    }

    public void setFatherId(long fatherId) {
        this.fatherId = fatherId;
    }

}

