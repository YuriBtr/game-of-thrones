package ru.skillbranch.yuribtr.data.database;

import java.util.ArrayList;
import java.util.List;

import ru.skillbranch.yuribtr.data.storage.models.DaoSession;
import ru.skillbranch.yuribtr.data.storage.models.House;
import ru.skillbranch.yuribtr.data.storage.models.HouseDao;
import ru.skillbranch.yuribtr.utils.SkillBranchApplication;
import ru.skillbranch.yuribtr.utils.UiHelper;

public class LoadHousesFromDb {
    private DaoSession mDaoSession;

    public List<House> getHouses() {
        UiHelper.writeLog("LoadHousesFromDb called");
        mDaoSession = SkillBranchApplication.getDaoSession();
        List<House> houseList = new ArrayList<>();
        try {
            houseList = mDaoSession.queryBuilder(House.class)
                    .orderDesc(HouseDao.Properties.Name)
                    .build()
                    .list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return houseList;
    }

}