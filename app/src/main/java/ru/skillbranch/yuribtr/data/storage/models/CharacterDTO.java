package ru.skillbranch.yuribtr.data.storage.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Unique;

public class CharacterDTO implements Parcelable {

    @Id
    private Long id;

    @NotNull
    @Unique
    private Long remoteId;

    private String houseRemoteId;

    private String houseShortName;

    private String url;

    private String name;

    private String gender;

    private String culture;

    private String born;

    private String died;

    private String titles;

    private String aliases;

    private String father;

    private String mother;

    private String spouse;

    private String allegiances;

    private String books;

    private String povBooks;

    private String tvSeries;

    private String playedBy;

    private String houseWords;

    private CharacterDTO fatherCharacter;

    private CharacterDTO motherCharacter;

    public void setFatherCharacter(CharacterDTO fatherCharacter) {
        this.fatherCharacter = fatherCharacter;
    }

    public void setMotherCharacter(CharacterDTO motherCharacter) {
        this.motherCharacter = motherCharacter;
    }

    public CharacterDTO getFatherCharacter() {
        return fatherCharacter;
    }

    public CharacterDTO getMotherCharacter() {
        return motherCharacter;
    }

    public String getHouseWords() {
        return houseWords;
    }

    public void setHouseWords(String houseWords) {
        this.houseWords = houseWords;
    }

    public void setHouseShortName(String houseShortName) {
        this.houseShortName = houseShortName;
    }

    public String getHouseShortName() {
        return houseShortName;
    }

    public CharacterDTO(Long id, Long remoteId, String houseRemoteId, String houseShortName, String url,
                        String name, String gender, String culture, String born, String died, String titles,
                        String aliases, String father, String mother, String spouse, String allegiances,
                        String books, String povBooks, String tvSeries, String playedBy, String houseWords,
                        CharacterDTO fatherCharacter, CharacterDTO motherCharacter) {
        this.id = id;
        this.remoteId = remoteId;
        this.houseRemoteId = houseRemoteId;
        this.houseShortName = houseShortName;
        this.url = url;
        this.name = name;
        this.gender = gender;
        this.culture = culture;
        this.born = born;
        this.died = died;
        this.titles = titles;
        this.aliases = aliases;
        this.father = father;
        this.mother = mother;
        this.spouse = spouse;
        this.allegiances = allegiances;
        this.books = books;
        this.povBooks = povBooks;
        this.tvSeries = tvSeries;
        this.playedBy = playedBy;
        this.houseWords = houseWords;
        this.fatherCharacter = fatherCharacter;
        this.motherCharacter = motherCharacter;
    }

    public CharacterDTO() {
    }

    public CharacterDTO(Character character) {
        super();
        this.id = character.getId();
        this.remoteId = character.getRemoteId2();
        this.houseRemoteId = character.getHouseRemoteId();
        this.houseShortName = character.getHouseShortName();
        this.url = character.getUrl();
        this.name = character.getName();
        this.gender = character.getGender();
        this.culture = character.getCulture();
        this.born = character.getBorn();
        this.died = character.getDied();
        this.titles = character.getTitles();
        this.aliases = character.getAliases();
        this.father = character.getFather();
        this.mother = character.getMother();
        this.spouse = character.getSpouse();
        this.allegiances = character.getAllegiances();
        this.books = character.getBooks();
        this.povBooks = character.getPovBooks();
        this.tvSeries = character.getTvSeries();
        this.playedBy = character.getPlayedBy();
        this.houseWords = character.getHouseWords();
        if (character.getFatherCharacter() != null)
            this.fatherCharacter = new CharacterDTO(character.getFatherCharacter());
        if (character.getMotherCharacter() != null)
            this.motherCharacter = new CharacterDTO(character.getMotherCharacter());
    }

    /**
     * @return The url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender The gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return The culture
     */
    public String getCulture() {
        return culture;
    }

    /**
     * @param culture The culture
     */
    public void setCulture(String culture) {
        this.culture = culture;
    }

    /**
     * @return The born
     */
    public String getBorn() {
        return born;
    }

    /**
     * @param born The born
     */
    public void setBorn(String born) {
        this.born = born;
    }

    /**
     * @return The died
     */
    public String getDied() {
        return died;
    }

    /**
     * @param died The died
     */
    public void setDied(String died) {
        this.died = died;
    }

    /**
     * @return The titles
     */
    public String getTitles() {
        return titles;
    }

    /**
     * @param titles The titles
     */
    public void setTitles(String titles) {
        this.titles = titles;
    }

    /**
     * @return The aliases
     */
    public String getAliases() {
        return aliases;
    }

    /**
     * @param aliases The aliases
     */
    public void setAliases(String aliases) {
        this.aliases = aliases;
    }

    /**
     * @return The father
     */
    public String getFather() {
        return father;
    }

    /**
     * @param father The father
     */
    public void setFather(String father) {
        this.father = father;
    }

    /**
     * @return The mother
     */
    public String getMother() {
        return mother;
    }

    /**
     * @param mother The mother
     */
    public void setMother(String mother) {
        this.mother = mother;
    }

    /**
     * @return The spouse
     */
    public String getSpouse() {
        return spouse;
    }

    /**
     * @param spouse The spouse
     */
    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }

    /**
     * @return The allegiances
     */
    public String getAllegiances() {
        return allegiances;
    }

    /**
     * @param allegiances The allegiances
     */
    public void setAllegiances(String allegiances) {
        this.allegiances = allegiances;
    }

    /**
     * @return The books
     */
    public String getBooks() {
        return books;
    }

    /**
     * @param books The books
     */
    public void setBooks(String books) {
        this.books = books;
    }

    /**
     * @return The povBooks
     */
    public String getPovBooks() {
        return povBooks;
    }

    /**
     * @param povBooks The povBooks
     */
    public void setPovBooks(String povBooks) {
        this.povBooks = povBooks;
    }

    /**
     * @return The tvSeries
     */
    public String getTvSeries() {
        return tvSeries;
    }

    /**
     * @param tvSeries The tvSeries
     */
    public void setTvSeries(String tvSeries) {
        this.tvSeries = tvSeries;
    }

    /**
     * @return The playedBy
     */
    public String getPlayedBy() {
        return playedBy;
    }

    /**
     * @param playedBy The playedBy
     */
    public void setPlayedBy(String playedBy) {
        this.playedBy = playedBy;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHouseRemoteId() {
        return this.houseRemoteId;
    }

    public void setHouseRemoteId(String houseRemoteId) {
        this.houseRemoteId = houseRemoteId;
    }

    public Long getRemoteId() {
        return this.remoteId;
    }

    public void setRemoteId(Long remoteId) {
        this.remoteId = remoteId;
    }


    protected CharacterDTO(Parcel in) {
        id = in.readByte() == 0x00 ? null : in.readLong();
        remoteId = in.readByte() == 0x00 ? null : in.readLong();
        houseRemoteId = in.readString();
        houseShortName = in.readString();
        url = in.readString();
        name = in.readString();
        gender = in.readString();
        culture = in.readString();
        born = in.readString();
        died = in.readString();
        titles = in.readString();
        aliases = in.readString();
        father = in.readString();
        mother = in.readString();
        spouse = in.readString();
        allegiances = in.readString();
        books = in.readString();
        povBooks = in.readString();
        tvSeries = in.readString();
        playedBy = in.readString();
        houseWords = in.readString();
        fatherCharacter = (CharacterDTO) in.readValue(CharacterDTO.class.getClassLoader());
        motherCharacter = (CharacterDTO) in.readValue(CharacterDTO.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(id);
        }
        if (remoteId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(remoteId);
        }
        dest.writeString(houseRemoteId);
        dest.writeString(houseShortName);
        dest.writeString(url);
        dest.writeString(name);
        dest.writeString(gender);
        dest.writeString(culture);
        dest.writeString(born);
        dest.writeString(died);
        dest.writeString(titles);
        dest.writeString(aliases);
        dest.writeString(father);
        dest.writeString(mother);
        dest.writeString(spouse);
        dest.writeString(allegiances);
        dest.writeString(books);
        dest.writeString(povBooks);
        dest.writeString(tvSeries);
        dest.writeString(playedBy);
        dest.writeString(houseWords);
        dest.writeValue(fatherCharacter);
        dest.writeValue(motherCharacter);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CharacterDTO> CREATOR = new Parcelable.Creator<CharacterDTO>() {
        @Override
        public CharacterDTO createFromParcel(Parcel in) {
            return new CharacterDTO(in);
        }

        @Override
        public CharacterDTO[] newArray(int size) {
            return new CharacterDTO[size];
        }
    };
}