package ru.skillbranch.yuribtr.data.database;

import android.content.Context;
import android.support.annotation.NonNull;
import com.redmadrobot.chronos.ChronosOperation;
import com.redmadrobot.chronos.ChronosOperationResult;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import ru.skillbranch.yuribtr.R;
import ru.skillbranch.yuribtr.data.managers.DataManager;
import ru.skillbranch.yuribtr.data.network.res.CharacterRes;
import ru.skillbranch.yuribtr.data.network.res.HouseRes;
import ru.skillbranch.yuribtr.data.storage.models.CharacterDao;
import ru.skillbranch.yuribtr.data.storage.models.DaoSession;
import ru.skillbranch.yuribtr.data.storage.models.House;
import ru.skillbranch.yuribtr.data.storage.models.Character;
import ru.skillbranch.yuribtr.data.storage.models.HouseDao;
import ru.skillbranch.yuribtr.ui.activities.MainActivity;
import ru.skillbranch.yuribtr.ui.activities.SplashActivity;
import ru.skillbranch.yuribtr.utils.AppConfig;
import ru.skillbranch.yuribtr.utils.NetworkStatusChecker;
import ru.skillbranch.yuribtr.utils.SkillBranchApplication;
import ru.skillbranch.yuribtr.utils.UiHelper;
import ru.skillbranch.yuribtr.utils.UiUpdater;

public class ChronosSaveHousesToDb extends ChronosOperation<String> {
    private DataManager mDataManager;
    private Context mContext;
    private DaoSession mDaoSession;
    private HouseDao mHouseDao;
    private CharacterDao mCharacterDao;
    private String result="null";
    //sets preferred house numbers, and their q-ty (also this might be random)
    private String[] mHousesNumbers = {"378", "229", "362"};


    public ChronosSaveHousesToDb(String[] housesNumbers) {
        mHousesNumbers = housesNumbers;
    }

    public ChronosSaveHousesToDb() {
    }

    private void sendMessage(final String message){
        UiUpdater.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SplashActivity.UpdateMessage(message);
            }
        });
    }


    @Nullable
    @Override
    public String run() {
        //UiHelper.writeLog("ChronosSaveHousesToDb called");

        mContext= SkillBranchApplication.getContext();
        mDataManager = DataManager.getInstance();

        //if request was made less than 1 day, no loading will be done
        if (System.currentTimeMillis()-mDataManager.getPreferencesManager().getRefreshTime()< AppConfig.CACHE_TIME) {
           // UiHelper.writeLog("no needs to load data");
            result = mContext.getString(R.string.no_data_refresh_needed);
            sendMessage(mContext.getString(R.string.no_data_refresh_needed));
            try {
                Thread.sleep(AppConfig.START_DELAY);
            } catch (InterruptedException e) {
            }
            return result;
        }


        mDaoSession = mDataManager.getDaoSession();
        mHouseDao = mDaoSession.getHouseDao();
        mCharacterDao = mDaoSession.getCharacterDao();

        mHouseDao.deleteAll();
        mCharacterDao.deleteAll();

        if (NetworkStatusChecker.isNetworkAvailable(mContext)) {

            for (int i = 0; i< mHousesNumbers.length; i++) {
                try {
                    int k=i+1;
                    sendMessage(mContext.getString(R.string.loading_house_number)+k+mContext.getString(R.string.from_three));
                    Call<HouseRes> callHouse = mDataManager.getHouse(mHousesNumbers[i]);
                    Response<HouseRes> responseHouse = callHouse.execute();
                    if (responseHouse.code() == 200) {
                        House house = responseHouse.body().convertToHouse();
                        house.setRemoteId(mHousesNumbers[i]);

                        List<Character> characterList = new ArrayList<>();

                        for (String swornMembers : responseHouse.body().getSwornMembers()) {
                            //try to find indexes of members
                            int pos = swornMembers.lastIndexOf("/");
                            if (pos>=0 && pos+1<swornMembers.length()
                                    //BELOW LINE ONLY FOR DEBUG PURPOSES!!!!
                                    //&& ("1302".equals(swornMembers.substring(pos+1)) || "148".equals(swornMembers.substring(pos+1)) || "270".equals(swornMembers.substring(pos+1)) || "271".equals(swornMembers.substring(pos+1))  || "272".equals(swornMembers.substring(pos+1)) )
                                    ) {
                                //Thread.sleep(100);
                                Call<CharacterRes> callCharacter = mDataManager.getCharacter(swornMembers.substring(pos+1));
                                try {
                                    Response<CharacterRes> responseCharacter = callCharacter.execute();
                                    if (responseCharacter.code() == 200) {
                                        //adding character to house
                                        Character character = responseCharacter.body().convertToCharacter();
                                        character.setHouseRemoteId(house.getRemoteId());
                                        character.setHouseShortName(house.getHouseShortName());
                                        character.setHouseWords(house.getWords());
                                        characterList.add(character);
                                    } else if (responseCharacter.code() == 404) {
                                        result = mContext.getString(R.string.error_login_or_password);
                                        break;
                                    } else if (responseCharacter.code() == 401) {
                                        result = mContext.getString(R.string.error_token_message);
                                        break;
                                    } else {
                                        result = mContext.getString(R.string.error_unknown)+" "+responseCharacter.code();
                                        break;
                                    }
                                } catch (Exception e) {
                                    result = e.toString();
                                    break;
                                }
                            }
                        }
                        //saving characters if presents
                        if (characterList.size()>0) {
                            //retrieve and save character parents
                            for (Character character : characterList) {
                                if (character.getFather()!=null && !character.getFather().isEmpty()) {
                                    Character father = getCharacter(character.getFather());
                                    mCharacterDao.insertOrReplaceInTx(father);
                                    //setFatherCharacter must be called BEFORE insertOrReplaceInTx
                                    character.setFatherCharacter(father);
                                }
                                if (character.getMother()!=null && !character.getMother().isEmpty()) {
                                    Character mother = getCharacter(character.getMother());
                                    mCharacterDao.insertOrReplaceInTx(mother);
                                    //setMotherCharacter must be called BEFORE insertOrReplaceInTx
                                    character.setMotherCharacter(mother);
                                }
                            }
                            house.setSwornMembersList(characterList);
                            mCharacterDao.insertOrReplaceInTx(characterList);
                        }
                        //saving house
                        mHouseDao.insertOrReplaceInTx(house);
                        sendMessage(mContext.getString(R.string.success_houses_load));
                        mDataManager.getPreferencesManager().saveRefreshTime(System.currentTimeMillis());
                        result = mContext.getString(R.string.success_houses_load);
                    } else if (responseHouse.code() == 404) {
                        result = mContext.getString(R.string.error_login_or_password);
                        break;
                    } else if (responseHouse.code() == 401) {
                        result = mContext.getString(R.string.error_token_message);
                        break;
                    } else {
                        result = mContext.getString(R.string.error_unknown)+" "+responseHouse.code();
                        break;
                    }
                } catch (Exception e) {
                    result = e.toString();
                    break;
                }
            }

        } else {
            result=mContext.getString(R.string.error_network_is_not_available);
        }
        return result;
    }

    private Character getCharacter(String url) throws Exception {
        //Thread.sleep(100);
        Character result=null;
        //try to find indexes of members
        int pos = url.lastIndexOf("/");
        if (pos>=0 && pos+1<url.length()) {
            Call<CharacterRes> callCharacter = mDataManager.getCharacter(url.substring(pos+1));
            Response<CharacterRes> responseCharacter = callCharacter.execute();
            if (responseCharacter.code() == 200) {
                result = responseCharacter.body().convertToCharacter();
            } else if (responseCharacter.code() == 404) {
                throw new Exception(mContext.getString(R.string.error_login_or_password));
            } else if (responseCharacter.code() == 401) {
                throw new Exception(mContext.getString(R.string.error_token_message));
            } else {
                throw new Exception(mContext.getString(R.string.error_unknown) + " " + responseCharacter.code());
            }
        }
        return result;
    }


    @NonNull
    @Override
    public Class<? extends ChronosOperationResult<String>> getResultClass() {
        return Result.class;
    }

    public final static class Result extends ChronosOperationResult<String> {

    }
}

