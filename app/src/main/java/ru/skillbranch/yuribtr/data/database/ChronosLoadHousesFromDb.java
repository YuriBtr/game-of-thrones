package ru.skillbranch.yuribtr.data.database;

import android.support.annotation.NonNull;

import com.redmadrobot.chronos.ChronosOperation;
import com.redmadrobot.chronos.ChronosOperationResult;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

import ru.skillbranch.yuribtr.data.storage.models.DaoSession;
import ru.skillbranch.yuribtr.data.storage.models.House;
import ru.skillbranch.yuribtr.data.storage.models.HouseDao;
import ru.skillbranch.yuribtr.utils.SkillBranchApplication;
import ru.skillbranch.yuribtr.utils.UiHelper;

public class ChronosLoadHousesFromDb extends ChronosOperation<List<House>> {
    private DaoSession mDaoSession;


    @Nullable
    @Override
    public List<House> run() {
        UiHelper.writeLog("ChronosLoadHousesFromDb called");
        mDaoSession = SkillBranchApplication.getDaoSession();
        List<House> houseList = new ArrayList<>();
        try {
            houseList = mDaoSession.queryBuilder(House.class)
                    .orderDesc(HouseDao.Properties.Name)
                    .build()
                    .list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return houseList;
    }

    @NonNull
    @Override
    public Class<? extends ChronosOperationResult<List<House>>> getResultClass() {
        return Result.class;
    }

    public final static class Result extends ChronosOperationResult<List<House>> {
    }
}