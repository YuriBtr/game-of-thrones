This application provides Android code sample of usage "Game of Throns" API from site: http://anapioficeandfire.com
It was developed as test task for learning courses at http://skill-branch.ru


**Features**:

- Loading 3 houses (Starks, Lannisters, Targaryens)

- Loading all characters from these houses

- Loading all parents of characters from these houses

- Caching results at internal DB (default 1 day)


**Used techniques**:

- Retrofit2 + OkHTTP3

- JSON REST API

- Greendao ORM

- Chronos

- Stetho